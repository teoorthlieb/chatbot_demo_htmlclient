"use strict"

// Variables for chat and stored context specific events
var params = {}; // Object for parameters sent to the Koios Conversation service
var watson = 'Koïos'
var user = ''
var text = ''
var context
var id = makeid()
var webhook = "http://localhost:5000/"
var old = 'https://serene-tundra-80387.herokuapp.com/'
var lang = "en"

function change_language(el){
    var value = el.options[el.selectedIndex].value;
    lang = value;
    console.log("new lang: "+lang);
    delete_previous();
    userMessage("resend");
}


function delete_previous(){
    var chatbox = document.getElementById("chatBox");
    var lastmsg = chatbox.lastChild;
    var title = lastmsg.children[0];
    if(title.className == "anaTitle"){
        chatbox.removeChild(chatbox.lastChild);
    }
}
// Make unique ID for user
function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 25; i++){
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

/**
* @summary Enter Keyboard Event.
*
* When a user presses enter in the chat input window it triggers the service interactions.
*
* @function newEvent
* @param {Object} e - Information about the keyboard event.
*/
function newEvent(e) {
    // Only check for a return/enter press - Event 13
    if (e.which === 13 || e.keyCode === 13) {

        var userInput = document.getElementById('chatMessage');
        text = userInput.value; // Using text as a recurring variable through functions
        text = text.replace(/(\r\n|\n|\r)/gm, ""); // Remove erroneous characters

        // If there is any input then check if this is a claim step
        // Some claim steps are handled in newEvent and others are handled in userMessage
        if (text) {

            // Display the user's text in the chat box and null out input box
            displayMessage(text, user);
            userInput.value = '';

            userMessage(text);

        } else {

            // Blank user message. Do nothing.
            console.error("No message.");
            userInput.value = '';

            return false;
        }
    }
}


function reload_old() {
    // Set the lang according to the langselector
    var selector = document.getElementById("langselector");
    lang = selector.options[selector.selectedIndex].value;
    console.log("RELOAD");
    // Reload the database on page load
    params.payload = "quit"; // User defined text to be sent to service
    params.user_time = new Date();
    params.user_id = id;
    params.lang = lang;

    if (context) {
        params.context = context;
    }

    var xhr = new XMLHttpRequest();
    var uri = webhook;
    //var uri = 'http://127.0.0.1:5000/'
    //var uri = 'https://powerful-springs-45559.herokuapp.com/'

    xhr.open('POST', uri, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader('Access-Control-Allow-Origin', webhook);
    xhr.onload = function() {

        // Verify if there is a success code response and some text was sent
        if (xhr.status === 200 && xhr.responseText) {
            //if (xhr.status === 200) {
            //displayMessage(xhr.status)

            var response = JSON.parse(xhr.responseText);
            text = response.text; // Only display the first response

            // IMPORTANT STUFF HAPPENS HERE!!!!!!!!!!!!!!!!!
            // Check if there is quick replies for the answer
            var quick_replies = response.quick_replies;

            //displayMessage(quick_replies, watson)
            //context = response.context; // Store the context for next round of questions
            //text = 'ehieqwijd'

            //displayMessage(text, watson, quick_replies)

        } else {
            console.error('Server error for Conversation. Return status of: ', xhr.statusText);
            //displayMessage("I ran into an error. Could you please try again.", watson)
        }
    }

    xhr.onerror = function() {
        //displayMessage(xhr.status)
        console.error('Network error trying to send message!');
        //displayMessage("I can't reach my brain right now. Try again in a few minutes.", watson)
    }

    xhr.send(JSON.stringify(params));
    var welcomeText = "Hello! I'm Koïos, you can ask me any questions you'd like, or I can help you find the right product for your needs!";
    var welcomeReply = [{
        'content_type': "text",
        "title": "Find me an insurance!",
        "message_sent": "I want to find an insurance.",
        "payload": undefined
    }];
    displayMessage(welcomeText, watson, welcomeReply);

}

function reload(){
    var selector = document.getElementById("langselector");
    lang = selector.options[selector.selectedIndex].value;
    console.log("RELOAD");
    userMessage("start");
}
/**
* @summary Main User Interaction with Service.
*
* Primary function for parsing the conversation context  object, updating the list of
* variables available to Koios, handling when a conversation thread ends and resetting the
* context, and kicking off log generation.
*
* @function userMessage
* @param {String} message - Input message from user or page load.
*/
function userMessage(message) {

    // Set parameters for payload to koios Conversation
    params.payload = message; // User defined text to be sent to service
    params.user_time = new Date();
    params.user_id = id;
    params.lang = lang;
    if (context) {
        params.context = context;
    }

    var xhr = new XMLHttpRequest();
    var uri = webhook;
    //var uri = 'http://127.0.0.1:5000/'

    xhr.open('POST', uri, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader('Access-Control-Allow-Origin', webhook);
    xhr.onload = function() {

        // Verify if there is a success code response and some text was sent
        if (xhr.status === 200 && xhr.responseText) {
            //if (xhr.status === 200) {
            //displayMessage(xhr.status)

            var response = JSON.parse(xhr.responseText)
            text = response.message; // Only display the first response

            // IMPORTANT STUFF HAPPENS HERE!!!!!!!!!!!!!!!!!
            // Check if there is quick replies for the answer
            var quick_replies = response.payload;

            //displayMessage(quick_replies, watson)
            //context = response.context; // Store the context for next round of questions
            //text = 'ehieqwijd'

            displayMessage(text, watson, quick_replies);

        } else {
            console.error('Server error for Conversation. Return status of: ', xhr.statusText);
            displayMessage("I ran into an error. Could you please try again.", watson);
        }
    }

    xhr.onerror = function() {
        //displayMessage(xhr.status)
        console.error('Network error trying to send message!');
        displayMessage("I can't reach my brain right now. Try again in a few minutes.", watson);
    }

    xhr.send(JSON.stringify(params));
}

function getTimestamp() {
    var d = new Date();
    var hours = d.getHours();
    var minutes = d.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

/**
* @summary Display Chat Bubble.
*
* Formats the chat bubble element based on if the message is from the user or from Koios.
*
* @function displayMessage
* @param {String} text - Text to be dispalyed in chat box.
* @param {String} user - Denotes if the message is from Koios or the user.
* @return null
*/
function displayMessage(text, user, quick_replies=null) {
    var chat = document.getElementById('chatBox');
    var bubble = document.createElement('div');
    bubble.className = 'message'; // Wrap the text first in a message class for common formatting

    // Set chat bubble color and position based on the user parameter
    if (user === watson) {
        var name = "Koïos";
        var html = `<div class="anaTitle">${name} | ${getTimestamp()} <\/div><div class="ana">${text}<\/div>`;
        bubble.innerHTML = html.replace(/\n/, "\\n");
        if (quick_replies !== null) {
            // Testing some stuff - adding quick replies to bubble
            var quickReplies = document.createElement('div');
            for (var i = 0; i < quick_replies.length; i++) {
                var tosend = quick_replies[i]['message_sent'];
                if (typeof tosend == "string"){
                    tosend = replaceAll(tosend, "'", "\\'");
                    console.log(">>>> "+tosend);
                }
                var payload = quick_replies[i]['payload'];
                if (typeof payload == "string"){
                    payload = replaceAll(payload, "'", "\\'");
                }

                var title = quick_replies[i]['title'];
                var html = `<button class="button_chat ana" onclick="sendQuickReply('${tosend}', '${payload}')">${title}<\/button>`;
                console.log(html);
                quickReplies.innerHTML += html.replace(/\n/, "\\n");
            }

            bubble.appendChild(quickReplies);
        }
    } else {
        var name = "User";
        if(context && context.fname && context.fname.length > 0){
            name = context.fname;
        }
        var html = `<div class="userTitle">${name} | ${getTimestamp()}<\/div><div class="user">${text}<\/div>`;
        bubble.innerHTML = html.replace(/\n/, "\\n");
    }

    chat.appendChild(bubble);

    chat.scrollTop = chat.scrollHeight; // Move chat down to the last message displayed
    document.getElementById('chatMessage').focus();
}

function sendQuickReply(message, ans) {
    displayMessage(message);
    userMessage(ans);
    [].forEach.call(document.querySelectorAll('.button_chat'), function (el) {
        el.parentNode.removeChild(el);
    });
}
